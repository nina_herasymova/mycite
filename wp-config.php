<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'u7`Z-8Su-@~cIIYsM+FTR~wmPB<x[4ra@tT7[vI-[CBfb$-S8;BBYWVlx. I MY-' );
define( 'SECURE_AUTH_KEY',  'c=k6`B^g#hX]^;bvi+-0>j2/q,]3NkOW=6{|yES$ne@2|gecA!%k{pWY!/,&#iqM' );
define( 'LOGGED_IN_KEY',    '^Xfj&DA~3B/7pn49UM3(NUAkuzs@Tad}up@1:wat.g=gOD|s*}-jviL]?Z(zx D_' );
define( 'NONCE_KEY',        '0,@& L}3P2@nl%`^hDUKL6F=55a_;jHaYVuk1V@@_@-FR7#(L2f9)}]/DUR{xPY5' );
define( 'AUTH_SALT',        '%v[O4Gsd2a,Q#l)mo6$@!9l3{s/avtKbeIgI5T98v[w`X8Gy8m24]$Q/9DqlemLv' );
define( 'SECURE_AUTH_SALT', '90$q6MwwG.`c,n>j8.KA;2AqNQKe<vy6U#w`k0lS;EUA0F51[s=0h$G0n^w^PUL9' );
define( 'LOGGED_IN_SALT',   '/CT+B1!aw,J:]:Ut1pqe<lA<6OO4|S}dd?cGOK8W)Ns_6Y?v)xTF/fu(aT<$=>g}' );
define( 'NONCE_SALT',       'KmKPeQxjTcMFpntb`!I$wdTtw%`NWHL&@`+)knK.Nhf=cF>a PussR3D)XfU<.7e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
