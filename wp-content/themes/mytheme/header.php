<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri() ?>"/>
  <?php wp_head() ?>

</head>
<body <?php body_class();
wp_nav_menu(['menu' => 'Site navigation']); ?>
