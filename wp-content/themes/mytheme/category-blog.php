<?php get_header(); ?>
<div class="content blog"><?php
  if (have_posts()) {
    while (have_posts()) {

      the_post();
//      the_date();
      the_title('<h1 class="title">', '</h1>');
      the_content();
    };
  }; ?>
</div>

<?php get_footer(); ?>
