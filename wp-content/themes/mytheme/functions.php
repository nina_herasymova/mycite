<?php
wp_register_style('my_style', get_template_directory_uri() . 'style/css' . array('my_reset'), '1.47', 'screen');

wp_enqueue_style('my_style');

add_action('after_setup_theme', 'theme_register_nav_menu');

add_theme_support('title-tag');

function theme_register_nav_menu()
{
  register_nav_menu('primary', 'Primary Menu');
}